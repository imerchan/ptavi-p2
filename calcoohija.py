#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):
    def mult(self):
        """ Function to multiply the operands. Ops have to be ints """
        self.solucion = self.operando1 * self.operando2

    def div(self):
        """ Function to split the operands """
        try:
            self.solucion = self.operando1/self.operando2
        except ZeroDivisionError:
            sys.exit("Division by zero is not allowed")

    def operar2(self):
        # Paso el objeto calc luego
        if self.operador == "divide":
            CalculadoraHija.div(self)
        elif self.operador == "multiplica":
            CalculadoraHija.mult(self)
        else:
            sys.exit('Operación sólo puede ser divide o mult.')


if __name__ == "__main__":

    if len(sys.argv) != 4:
        print()
        sys.exit("Usage : $ python3 operando1 operador operando2")
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")
    operador = sys.argv[2]
    Calc = CalculadoraHija(operador, operando1, operando2)
    if (operador == "suma") or (operador == "resta"):
        Calc.operar()
    elif operador == "multiplica" or (operador == "divide"):
        CalculadoraHija.operar2(Calc)
    else:
        sys.exit('Operación sólo puede ser suma, resta, multiplica o divide')

    print(Calc.solucion)
