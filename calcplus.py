#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoohija


if __name__ == "__main__":

    fd = open(sys.argv[1], mode='r', encoding='utf-8')
    for line in fd.readlines():
        try:
            lista_calc = line.split(",")
            lista_int = list(map(int, lista_calc[1:]))

            operando1 = int(lista_int[0])
            for op2 in lista_int[1:]:
                operador = lista_calc[0]
                operando2 = int(op2)
                Calc = calcoohija.CalculadoraHija(operador, operando1, operando2)
                if Calc.operador == "suma":
                    Calc.suma()
                    operando1 = Calc.solucion
                    result = operando1
                elif Calc.operador == "resta":
                    Calc.resta()
                    operando1 = Calc.solucion
                    result = operando1
                elif Calc.operador == "multiplica":
                    Calc.mult()
                    operando1 = Calc.solucion
                    result = operando1
                elif Calc.operador == "divide":
                    if operando2 == 0:
                        sys.exit('Division by zero is not allowed')
                    else:
                        Calc.div()
                        operando1 = Calc.solucion
                        result = operando1
                else:
                    sys.exit('Operación sólo puede ser suma,resta,multiplica o divide')
        except ValueError:
            sys.exit('Error: Non numerical parameters')

        print(result)
    fd.close()
