#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoohija
import csv


if __name__ == "__main__":
    Calc = calcoohija.CalculadoraHija
    with open(sys.argv[1]) as csvarchivo:
        fichero = csv.reader(csvarchivo)
        operaciones = {"suma": Calc.suma,
                       "resta": Calc.resta,
                       "multiplica": Calc.mult,
                       "divide": Calc.div}
        for line in fichero:
            try:
                lista_calc = line
                lista_int = list(map(int, line[1:]))

                op1 = lista_int[0]
                for op2 in lista_int[1:]:
                    try:
                        if op2 == 0 and lista_calc[0] == "divide":
                            result = "Division by zero is not allowed"
                            break
                        else:
                            Calc.operando1 = op1
                            Calc.operando2 = int(op2)
                            operaciones[lista_calc[0]](Calc)
                            op1 = Calc.solucion
                            result = op1
                    except KeyError:
                        result = "Sólo puede ser suma,resta,mult. o divide."
            except ValueError:
                result = "Error: Non numerical parameters"

            print(result)
